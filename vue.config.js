// vue.config.js
module.exports = {
  devServer: {
    watchOptions: {
      poll: 1000,
      aggregateTimeout: 1000
    }
  },
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  }
};
