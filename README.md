# Ant DataGrid Table

## Repository

https://gitlab.com/omniait/ant-datagrid-table

## Features

* Ant Design Vue based table
* Typescript based
* Modular Vue component



- [ ] Fixed header and column with reactive prop values (one way data-binding)
- [ ] Sorting for various data types (numbers, dates, strings)
- [ ] Searching with highlighting
- [ ] (C)RUD operations against a REST API (Create has the least priority)
- [ ] Import/Export to .csv
- [ ] Import/Export to .pdf and .odt

## Getting started

### Project setup

```
yarn install
```

###  Compiles and hot-reloads for development

```
yarn run serve
```

###  Compiles and minifies for production

```
yarn run build
```

###  Run your tests

```
yarn run test
```

###  Lints and fixes files

```
yarn run lint
```

###  Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

